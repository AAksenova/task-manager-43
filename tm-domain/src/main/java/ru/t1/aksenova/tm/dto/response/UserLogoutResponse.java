package ru.t1.aksenova.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public final class UserLogoutResponse extends AbstractResultResponse {

    public UserLogoutResponse(@NotNull Throwable error) {
        super(error);
    }

}
