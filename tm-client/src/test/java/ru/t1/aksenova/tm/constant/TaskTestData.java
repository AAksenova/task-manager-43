package ru.t1.aksenova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public class TaskTestData {

    @NotNull
    public final static String ALFA_TASK_NAME = "Alfa task";

    @NotNull
    public final static String ALFA_TASK_DESCRIPTION = "Alfa task description";

    @NotNull
    public final static String BETTA_TASK_NAME = "Betta task";

    @NotNull
    public final static String BETTA_TASK_DESCRIPTION = "Betta task description";

    @NotNull
    public final static String GAMMA_TASK_NAME = "Gamma task";

    @NotNull
    public final static String GAMMA_TASK_DESCRIPTION = "Gamma task description";

}
